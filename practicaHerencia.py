class Persona:
    def __init__(self, nombre, apellidos, fecha, DNI):
        self._nombre = nombre
        self._apellidos = apellidos
        self._fecha = fecha
        self._DNI = DNI

    def setNombre(self, nombre):
        self._nombre = nombre

    def getNombre(self):
        return self._nombre 

    def setApellidos(self, apellidos):
        self._apellidos = apellidos

    def getApellidos(self):
        return self._apellidos

    def setFecha(self, fecha):
        self._fecha = fecha

    def getFecha(self):
        return self._fecha 

    def setDNI(self, DNI):
        self._DNI = DNI

    def getDNI(self):
        return self._DNI 

    def __str__(self):
        #return "nombre: " + str(nombre) + " ,apellido:" + str(apellidos) + "y fecha de nacimiento" + srt(fecha) + "con DNI" + srt(DNI)
        return f'Nombre: {self._nombre} , Apellido: {self._apellidos}, fecha de nacimiento {self._fecha} y DNI: {self._DNI}'


class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha, DNI, historial):
        super().__init__(nombre, apellidos, fecha, DNI)
        self._historialC= historial


    def ver_historial_clinico(self):
        return 'Historial clínico: {historial}'.format(historial = self._historialC)
    
class Médico (Persona):
    def __init__(self, nombre, apellidos, fecha, DNI, especial, cit):
        super().__init__(nombre, apellidos, fecha, DNI)
        self._especialidad = especial
        self._cita = cit

    def consultar_agenda(self):
        return 'Cita programada el {cit} en la especialidad {especial}'.format(cit = self._cita, especial= self._especialidad)
    
persona = Persona("Paula", "Garcia", "20/12/2003", "567890907B")
paciente = Paciente("Sara", "Barbacoa", "03/09/1990", "895647345P", "demencia")
medico = Médico("Che", "Logarci", "27/06/1977", "289354672J", "psicología", "25/04/2024 a las 12:45")

people = [persona, paciente, medico]
for elemento in people:
    print(elemento)
    if elemento == paciente:
        print (elemento.ver_historial_clinico())
    elif elemento == medico:
        print(elemento.consultar_agenda())