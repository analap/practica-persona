from practicaHerencia import Persona 
from practicaHerencia import Paciente
from practicaHerencia import Médico
import unittest

class TestHerencia(unittest.TestCase):

    def test_historial_clinico(self):
        paciente = Paciente("Sara", "Barbacoa", "03/09/1990", "895647345P", "demencia")
        historial = paciente.ver_historial_clinico()
        self.assertEqual(historial, "Historial clínico: demencia")

    def test_consultar_agenda(self):
        medico=Médico("Che", "Logarci", "27/06/1977", "289354672J", "psicología", "25/04/2024 a las 12:45")
        agenda = medico.consultar_agenda()
        self.assertEqual(agenda, 'Cita programada el 25/04/2024 a las 12:45 en la especialidad psicología')

if __name__ == "__main__":
    unittest.main()